package dummydao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class DummyDao {
	
	public static void main(String[] args) {
	    final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
	    executorService.scheduleAtFixedRate(new Runnable() {
	        @Override
	        public void run() {
	            try {
					getPrice();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	    }, 0, 1, TimeUnit.SECONDS);
	}
	
	public static Runnable getPrice() throws IOException {
		
		double currentStockPrice = 0;
		String inputline;
		
		long CURRENT_TIME_MILLIS = System.currentTimeMillis();
		Date instant = new Date(CURRENT_TIME_MILLIS);
	     SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm:ss" );
	     String time = sdf.format( instant );
		
		URL google = new URL("http://feed.conygre.com:8080/MockYahoo/quotes.csv?s=goog&f=p0");
		BufferedReader googin = new BufferedReader(new InputStreamReader(google.openStream()));
			while ((inputline = googin.readLine()) != null)
			currentStockPrice = Double.parseDouble(inputline);
			System.out.println( "Time: " + time + " Current Google Stock price: £" + currentStockPrice);
			
			URL msft = new URL("http://feed.conygre.com:8080/MockYahoo/quotes.csv?s=msft&f=p0");
			BufferedReader mftin = new BufferedReader(new InputStreamReader(msft.openStream()));
				while ((inputline = mftin.readLine()) != null)
				currentStockPrice = Double.parseDouble(inputline);
				System.out.println( "Time: " + time + " Current Microsoft Stock price: £" + currentStockPrice);
				
		return null;
	}

}
