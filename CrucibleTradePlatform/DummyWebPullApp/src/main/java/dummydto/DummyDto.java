package dummydto;

public class DummyDto {
	
	private int currentStockPrice;

	public DummyDto(int currentStockPrice) {
		super();
		this.currentStockPrice = currentStockPrice;
	}

	public int getCurrentStockPrice() {
		return currentStockPrice;
	}
	public void setCurrentStockPrice(int currentStockPrice) {
		this.currentStockPrice = currentStockPrice;
	}

	@Override
	public String toString() {
		return "DummyDto [currentStockPrice=" + currentStockPrice + "]";
	}

}
