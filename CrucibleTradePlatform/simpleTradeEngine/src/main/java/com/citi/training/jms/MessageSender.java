package com.citi.training.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import com.citi.training.data.entities.Trade;

@Component
public class MessageSender {
	
	@Autowired
	private JmsTemplate jmsTemplate;

	public void sendTrade(Trade trade) {		
		String messageToSend = Trade.toXml(trade);
		System.out.println("Sending: "+ messageToSend);
		jmsTemplate.convertAndSend("OrderBroker", messageToSend,
				message -> {
					message.setStringProperty("Operation", "Update");
					message.setJMSCorrelationID(trade.getId().toString());
					return message;
				});
	}
	
//	public String tradeXML(
//			TradeType tradeType, 
//			int id, 
//			double price, 
//			int size, 
//			String stock, 
//			Date whenAsDate) {
//		
//		StringBuilder tradeXML = new StringBuilder();
//		
//		tradeXML.append("<trade>\n");
//		tradeXML.append("<buy>"+tradeType+"</buy>\n");
//		tradeXML.append("<id>"+id+"</id>\n");
//		tradeXML.append("<price>"+price+"</price>\n");
//		tradeXML.append("<size>"+size+"</size>\n");
//		tradeXML.append("<stock>"+stock+"</stock>\n");
//		tradeXML.append("<whenAsDate>"+whenAsDate+"</whenAsDate>\n");
//		tradeXML.append("</trade>");
//		
//		return tradeXML.toString();	
//	}
	
}
