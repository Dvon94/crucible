package com.citi.training.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.citi.training.data.entities.Trade;
import com.citi.training.data.services.TradeService;

@Component
public class MessageReceiver {
	
	@Autowired
	TradeService tradeBs;
	
	@JmsListener(destination="OrderBroker_Reply")
	public void receiveTrade(Message msg) {
	//	msg.getJMSCorrelationID()
		try {
			String tmsg = ((TextMessage) msg).getText();
			Trade trade = Trade.fromXml(tmsg);
			trade.setState(Trade.TradeState.FILLED);
			tradeBs.saveTrade(trade);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//xml reply
		
		//Trade.from
		
		//System.out.println("Received " + trade.toString());
	}

}
