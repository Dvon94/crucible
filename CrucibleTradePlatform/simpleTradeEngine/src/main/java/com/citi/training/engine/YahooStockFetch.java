package com.citi.training.engine;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class YahooStockFetch {

	private static int stockCount = 0;
	private static Double sum = 0.0;
	private static int arrayIteration = 0;

	private static ArrayList<Double> stockPrices = new ArrayList<Double>();

	public static void main(String[] args) {
		final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
		executorService.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				double currentStockPrice = getStockPrice("MSFT");
				while(stockPrices.size() > 60) {
					stockPrices.remove(stockCount);
				}
				stockPrices.add(stockCount, currentStockPrice);				
				stockCount++;
				if(stockCount == 15) {
					calcShortAverage(stockPrices);
				} else if(stockCount == 60) {
					calcLongAverage(stockPrices);					
					stockCount = 0;
					arrayIteration += 1;
				}

			}
		}, 0, 1, TimeUnit.SECONDS);
	}

	private static double calcAverage(ArrayList<Double> stockPrices) {	  
		if(!stockPrices.isEmpty()) {
			for (Double price : stockPrices) {
				sum += price;
			}
			System.out.println("AVERAGE "+sum);
			return sum.doubleValue() / stockPrices.size();
		}
		return sum;
	}

	public static double calcShortAverage(ArrayList<Double> stockPrices) {
		return calcAverage(stockPrices);
	}

	public static double calcLongAverage(ArrayList<Double> stockPrices) {
		return calcAverage(stockPrices);		
	}

	private static double getStockPrice(String stock) {

		double currentStockPrice = 0;
		String inputline;

		long CURRENT_TIME_MILLIS = System.currentTimeMillis();
		Date instant = new Date(CURRENT_TIME_MILLIS);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String time = sdf.format(instant);

		URL url;
		try {
			url = new URL("http://feed.conygre.com:8080/MockYahoo/quotes.csv?s="+stock+"&f=p0");
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			while ((inputline = reader.readLine()) != null)
				currentStockPrice = Double.parseDouble(inputline);

			System.out.println("Stock count: " + stockCount + " " + "Stock Price: " + currentStockPrice);								
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}

		return currentStockPrice;
	}

}