package com.citi.training.engine;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.data.entities.Trade;
import com.citi.training.data.services.TradeService;

@Component
public class TradeEngine {

    private static final Logger log = LoggerFactory.getLogger(TradeEngine.class);
	@Autowired
    Tester tester; 

    @Scheduled(fixedRate = 1000)
    public void sendTrades() {
    	
    	 tester.run();
    	
    	//get strat
    	//build trade from strats
    	//save the trade to table
    	//get price
    	//algo to buy or sell
    	
    	//tester.getprice
    	
//        List<Trade> initTrades = tradeService.getTradesByState(Trade.TradeState.INIT);
//
//        log.debug("Processing " + initTrades.size() + " new trades");
//        for(Trade trade : initTrades) {
//            trade.stateChange(Trade.TradeState.FILLED);
//            tradeService.saveTrade(trade);
//        }
    }
}
