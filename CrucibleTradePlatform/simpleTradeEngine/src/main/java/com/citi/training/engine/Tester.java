package com.citi.training.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.data.repository.StrategyDao;
import com.citi.training.data.services.StrategyBs;
import com.citi.training.data.services.TradeService;
import com.citi.training.jms.MessageReceiver;
import com.citi.training.jms.MessageSender;
import com.citi.training.data.entities.Strategy;
import com.citi.training.data.entities.Trade;

@Component
public class Tester {
	
	@Autowired
	StrategyBs strategyBs;
	
	@Autowired
	TradeService tradeBs;
	
	@Autowired
	MessageSender messageSender;
	
	@Autowired
	MessageReceiver messageReceiver;
	
	private final Logger logger = LogManager.getLogger(Tester.class);

	private final int SHORT_GAP_IN_SECONDS = 3;
	private final int LONG_GAP_IN_SECONDS = 6;

	private double[] shortArray = new double[SHORT_GAP_IN_SECONDS];
	private double[] longArray = new double[LONG_GAP_IN_SECONDS];

	private HashMap<Integer, Double> shortAverages = new HashMap<Integer, Double>();
	private HashMap<Integer, Double> longAverages = new HashMap<Integer, Double>();

	private int shortCount = 0;
	private int longCount = 0;
	private int priceCount = 0; 

	

	public void triggerBuy(String stock, double price, int quantity, int stdBuy, double shortAvg, double longAvg, int strategyId) {
		double buyCompareCondition = 
				longAvg - Math.sqrt((LONG_GAP_IN_SECONDS-1)*Math.pow(stdBuy, 2));

		if(buyCompareCondition > shortAvg) {
			logger.debug("Buy window open based on: "+buyCompareCondition+"(buy condition) > "+shortAvg+"(short average)");
			Trade trade = tradeBs.createTrade(stock, "BUY", price, quantity,strategyId);
			messageSender.sendTrade(trade);
			System.out.println("****BUY TRADE****");
		}
	}

	public void triggerSell(String stock, double price, int quantity, int stdSell, double shortAvg, double longAvg, int strategyId) {
		double sellCompareCondition = 
				longAvg + Math.sqrt((LONG_GAP_IN_SECONDS-1)*Math.pow(stdSell, 2));

		// Sell will happen if compareCondition < shortAvg
		if(sellCompareCondition < shortAvg) {
			logger.debug("Sell window open based on: "+sellCompareCondition+"(sell condition) > "+shortAvg+"(short average)");
			
			Trade trade = tradeBs.createTrade(stock, "SELL", price, quantity, strategyId);
			messageSender.sendTrade(trade);
			System.out.println("****SELL TRADE****");
			
			//send trade to mq orderbroker
			//messageSender.sendTrade(tradeBs.getTradeById(tradeId));
			
			
			// call activemq to trigger a buy	
			//messageReceiver.receiveTrade(tradeBs.getTradeById(tradeId));
			//Trade trade = tradeBs.createTrade(stock, "SELL");
			//messageSender.sendTrade(trade);
	
			//listener for the reply
			//messageReceiver.receiveTrade(tradeBs.getTradeById(tradeId));
			
			//save the trade to table as filled
			
			//fromxml to a trade object
			
			//tradeservice .save(trade)
			//tradeBs.saveTrade(tradeBs.getTradeById(tradeId));
			System.out.println("****SELL TRADE****");
		}
	}

	private double getStockPrice(String stock) {

		double currentStockPrice = 0;
		String inputline;

		long CURRENT_TIME_MILLIS = System.currentTimeMillis();
		Date instant = new Date(CURRENT_TIME_MILLIS);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String time = sdf.format(instant);

		URL url;
		try {
			url = new URL("http://feed.conygre.com:8080/MockYahoo/quotes.csv?s="+stock+"&f=p0");
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			while ((inputline = reader.readLine()) != null)
				currentStockPrice = Double.parseDouble(inputline);

			System.out.println(currentStockPrice);								
		} catch (NumberFormatException | IOException e) {
			logger.warn("Failed to obtain stock price, check your internet connection.", e);
		}

		return currentStockPrice;
	}

	private void processStrategy(Strategy strategy) throws InterruptedException {
		while(shortCount < SHORT_GAP_IN_SECONDS) {
			Thread.sleep(1000);
			shortArray[shortCount] = getStockPrice(strategy.getStock());
			longArray[longCount] = getStockPrice(strategy.getStock());

			if(priceCount > LONG_GAP_IN_SECONDS) {
				triggerBuy(
						strategy.getStock(), 
						getStockPrice(strategy.getStock()), 
						strategy.getStandardDeviationBuy(), 
						strategy.getStandardDeviationSell(), 
						shortAverages.get(priceCount), 
						longAverages.get(priceCount),
						strategy.getStrategyId()
						);
				
				triggerSell(
						strategy.getStock(), 
						getStockPrice(strategy.getStock()), 
						strategy.getStandardDeviationBuy(), 
						strategy.getStandardDeviationSell(), 
						shortAverages.get(priceCount), 
						longAverages.get(priceCount),
						strategy.getStrategyId()
						);
			}

			shortCount++;
			longCount++;
			priceCount++;		

			if(shortCount % SHORT_GAP_IN_SECONDS == 0) {
				logger.debug("Short average calculated based off of short average counter.");			
				shortAverages.put(priceCount, calcShortAverage(shortArray));
				System.out.println("shorts: "+shortAverages.get(priceCount));
				shortCount = 0;

				// ID (PK), DateTime (X axis), Y AXIS(Long avg, Short avg, current price) 
				if(priceCount >= LONG_GAP_IN_SECONDS) {
					// save short avg to db
					// new table
				}

			} else if(priceCount > SHORT_GAP_IN_SECONDS ) {
				logger.debug("Short average calculated based off of price counter.");
				shortAverages.put(priceCount, calcShortAverage(shortArray));
				System.out.println("shorts: "+shortAverages.get(priceCount));

				if(priceCount >= LONG_GAP_IN_SECONDS) {
					// save short avg to db
					// new table
				}

			}

			if(longCount % LONG_GAP_IN_SECONDS == 0) {
				logger.debug("Long average calculated based off of long average counter.");
				longAverages.put(priceCount, calcLongAverage(longArray));
				System.out.println("longs: "+longAverages.get(priceCount));
				longCount = 0;

				// save long avg to db
				// new table


			} else if(priceCount > LONG_GAP_IN_SECONDS) {
				logger.debug("Short average calculated based off of price counter.");
				longAverages.put(priceCount, calcLongAverage(longArray));
				System.out.println("longs: "+longAverages.get(priceCount));
				
				// save long avg to db
				// new table

			}			

		}
	}

	private double calcAverage(double[] stockPrices) {	  
		Double sum = 0.0;
		if(!(stockPrices.length == 0)) {
			for (Double price : stockPrices) {
				sum += price;
			}					
			return sum.doubleValue() / stockPrices.length;
		}
		return sum.doubleValue() / stockPrices.length;
	}

	public double calcShortAverage(double[] stockPrices) {
		return calcAverage(stockPrices);
	}

	public double calcLongAverage(double[] stockPrices) {
		return calcAverage(stockPrices);		
	}	
	
	public void run() {
		getStockPrice("GOOG");
		try {
			//processStrategy("GOOG", getStockPrice("GOOG"), 100, 0, 0);
			processStrategy(strategyBs.viewMostRecent());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	public static void main(String[] args) {
//		final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
//		executorService.scheduleAtFixedRate(new Runnable() {
//			@Override
//			public void run() {		
//				Tester tester = new Tester();
//
//			}
//		}, 0, 1, TimeUnit.SECONDS);
//	}

}
