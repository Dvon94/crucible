# This is a shell script to start the app from inside docker

# make log dir
mkdir -p log/

# elk config
if [[ -n $ELK ]]
then
        if ! grep elk.training.local /etc/hosts
        then
                echo "ADDING ELK ($ELK) TO /etc/hosts" >> /app/log/startapp.log
                echo "$ELK      elk.training.local" >>/etc/hosts
        else
                echo "UPDATING ELK AS ($ELK) TO /etc/hosts" >> /app/log/startapp.log
                sed -i "s/^.*elk\.training\.local.*/$ELK        elk.training.local/" /etc/hosts
        fi
fi

# logstash using /app/log
if [[ -d /opt/logstash ]] && grep 'elk\.training\.local' /etc/hosts >/dev/null 2>&1
then
        echo "STARTING LOGSTASH" >> /app/log/startapp.log
        # Start logstash
        /opt/logstash/bin/logstash -f /app/logstash-6.2.4/conf -l /app/log/logstash.log &
fi

# netprobe
if [[ -d /opt/itrs/netprobe ]]
then
        echo "STARTING NETPROBE" >> /app/log/startapp.log
        # Start netprobe
        export LOG_FILENAME=/app/log/netprobe.log
        /opt/itrs/netprobe/netprobe.linux_64 -port $NETPROBE &
fi

# start jar with stdin and stderr redirected to files in /app/log
java -Dspring.profiles.active=prod -jar  simpleTradeRestInterface-0.0.1-SNAPSHOT.jar 2 >> log/springwebapp.err 1 >> log/springwebapp.stdout
