/**
 * 
 */
$(document).ready(startup);

function startup() {
	// hookup trade searching events
	$("#buttonGetAllTrades").click(getAllTrades);
	$("#buttonTradesByStock").click(getTradeByStock);
}

function getAllTrades() {

	$.ajax({
			url : "/trades/tradeslist",
			dataType : "json",
			type : "GET",
			cache : false,
			async : false,
			contentType : 'application/json; charset=utf-8',

			success : function(response) {
				// alert(response[1].contactname);
				var output = "<Table><thead><tr> <th>Trade ID</th> <th>Trade Type</th> <th>Quantity of Stock Traded</th> <th>Stock Name</th> <th>Last State Change</th> <th>State</th></tr></thead>";
				for (var i=0; i < response.length; i++) {
					output+="<TR>";
					output+="<TD>" + response[i].id + "</td>";
					output+="<TD>" + response[i].tradeType + "</td>";
					output+="<TD>" + response[i].tradedQuantity + "</td>";
					output+="<TD>" + response[i].stock + "</td>";
					output+="<TD>" + response[i].lastStateChange + "</td>";
					output+="<TD>" + response[i].state + "</td>";
					output+="</TR>";
				}
				output +="</Table>";
				$("#tradeHistoryTableFormat").html(output);
			},
			error : function(err) {
				alert("Error: " + err.responseText)
			}
		});
}

function getTradeByStock() {

	var stockName = $("#textStockName").val();

	$.ajax({
		url : "/trades/stock/" + stockName ,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',

		
		success : function(response) {

			var output = "<Table><thead><tr> <th>Trade ID</th> <th>Trade Type</th> <th>Quantity of Stock Traded</th> <th>Stock Name</th> <th>Last State Change</th> <th>State</th></tr></thead>";
			for (var i=0; i < response.length; i++) {
				output+="<TR>";
				output+="<TD>" + response[i].id + "</td>";
				output+="<TD>" + response[i].tradeType + "</td>";
				output+="<TD>" + response[i].tradedQuantity + "</td>";
				output+="<TD>" + response[i].stock + "</td>";
				output+="<TD>" + response[i].lastStateChange + "</td>";
				output+="<TD>" + response[i].state + "</td>";
				output+="</TR>";
			}
			output +="</Table>";
			$("#tradeHistoryTableFormat").html(output);
		},
		error : function(err) {
			alert("Error: " + err.responseText)
		}
	});
}