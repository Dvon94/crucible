$(document).ready(startup);

function startup() {
	// hookup trade searching events
	$("#newStrategyIframe").show();
	$("#tradeHistoryIframe").show();
	$("#strategyHistoryIframe").show();
	
	$("#nsIframe").click(newSIframe);
	$("#thIframe").click(TradeHIframe);
	$("#shIframe").click(StrategyHIframe);
	
	google.charts.load('current', {packages: ['corechart', 'line']});
	google.charts.setOnLoadCallback(drawStockCharts);
	

}

function backtotop(){
	$
}


function newSIframe() {
	$("#newStrategyIframe").show();
	//$("#tradeHistoryIframe").hide();
	//$("#strategyHistoryIframe").hide();
}

function TradeHIframe() {
	$("#tradeHistoryIframe").show();
	//$("#newStrategyIframe").hide();
	//$("#strategyHistoryIframe").hide();
}

function StrategyHIframe() {
	$("#strategyHistoryIframe").show();
	//$("#newStrategyIframe").hide();
	//$("#tradeHistoryIframe").hide();
}



function drawStockCharts() {
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Current Price');
      data.addColumn('number', 'Short Avr');
      data.addColumn('number', 'Long Avr');

      data.addRows([
          [1,  37.8, 80.8, 41.8],
          [2,  30.9, 69.5, 32.4],
          [3,  25.4,   57, 25.7],
          [4,  11.7, 18.8, 10.5],
          [5,  11.9, 17.6, 10.4],
          [6,   8.8, 13.6,  7.7],
          [7,   7.6, 12.3,  9.6],
          [8,  12.3, 29.2, 10.6],
          [9,  16.9, 42.9, 14.8],
          [10, 12.8, 30.9, 11.6],
          [11,  5.3,  7.9,  4.7],
          [12,  6.6,  8.4,  5.2],
          [13,  4.8,  6.3,  3.6],
          [14,  4.2,  6.2,  3.4]
        ]);

      var options = {
    		  title: 'Stock Prices',
    		  titleTextStyle: {
    	            color: '#01579b',
    	            fontSize: 20,
    	            fontName: 'Arial',
    	            bold: true,
    	            italic: true
    	          },
        hAxis: {
          title: 'Time',
          textStyle: {
            color: '#01579b',
            fontSize: 20,
            fontName: 'Arial',
            bold: true,
            italic: true
          },
          titleTextStyle: {
            color: '#01579b',
            fontSize: 16,
            fontName: 'Arial',
            bold: false,
            italic: true
          }
        },
        vAxis: {
          title: 'Current Price',
          textStyle: {
            color: '#1a237e',
            fontSize: 24,
            bold: true
          },
          titleTextStyle: {
            color: '#1a237e',
            fontSize: 24,
            bold: true
          }
        },
        colors: ['#FFFF00','#a52714', '#097138']
      };
      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }


