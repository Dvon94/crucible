/**
 * 
 */
$(document).ready(startup);

function startup()
{
	getTradeControllerStatus();
	$("#btn_showAll".click(getTrades));
	}


function getTradeControllerStatus()
{
	 $.ajax({
	        url: "/trades/status",
	        //dataType: "json",
	        type: "GET",
	        cache: false,
	        async: false,
	        //contentType: 'application/json; charset=utf-8',
	        success: function (response) {
	        	$("#divStatus").html(response);
	           
	      },
	        error: function (err) {
	            alert("Error: " + err.responseText)
	        }
	    });
}

function getTrades()
{
	
	$.ajax({
        url: "/trades/getAllTrades",
        //data:params,
        dataType: "json",
        type: "GET",
        cache: false,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
        	//alert(response[1].contactname);
        	var output ="<Table><thead><tr><th>ID</th><th>Name</th></tr></thead>";
        	for (var i=0;i<response.length;i++)
        		{
        		output+="<TR>"
        		output+="<TD>" + response[i].id + "</td>";
        		output+="<TD>" + response[i].tradeType + "</td>";
        		output+="<TD>" + response[i].stock + "</td>";
        		output+="<TD>" + response[i].State + "</td>";
        		output+="<TD>" + response[i].lastStateChange + "</td>";
        		output+="</TR>"
        		}
        	output+="</Table>";
        	$("#panelResults").html(output);
           
      },
        error: function (err) {
            alert("Error: " + err.responseText)
        }
    });
}