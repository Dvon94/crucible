/**
 * 
 */

$(document).ready(startup);//end of ready

function startup()
{
	//getStratControllerStatus();
	//hookup events
	//$("#btn_findcust").click(getCustomerByID);
	//$("#btn_showall").click(getCustomers);
	
	$("#btn_save").click(save);
	
	 $( "#dialogSaved" ).dialog({
	      modal: true,
	      autoOpen:false,
	      buttons: {
	        Ok: function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
}

function getStratControllerStatus()
{
	 $.ajax({
	        url: "/strategies/status",
	        //dataType: "json",
	        type: "GET",
	        cache: false,
	        async: false,
	        //contentType: 'application/json; charset=utf-8',
	        success: function (response) {
	        	$("#divStatus").html(response);
	           
	      },
	        error: function (err) {
	            alert("Error: " + err.responseText)
	        }
	    });
}

function save()
{
	
	var xx=$("input[name='optradio']:checked").val();
	var rr=$("input[name='optradio1']:checked").val();
	
	var params={
			strategyId:-1,  
			stock:$("#drop_stock").val(),
			startingQuantity:$("#txt_Quantity").val(),
			budget:$("#txt_Budget").val(),
			standardDeviationSale:xx,
			standardDeviationBuy:rr,
			startDate:$("#txt_strt").val(),
			endDate:$("#txt_end").val()
			
	};
	var paramsJson = JSON.stringify(params);
	
	$.ajax({
        url: "/strategies/createStrategy",
        data:paramsJson,
        dataType: "json",
        type: "POST",
        cache: false,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
        	$("#dialogSaved").dialog('open');
        	//alert(response);
      },
        error: function (err) {
            alert("Error: " + err.responseText)
        }
    });
}    
