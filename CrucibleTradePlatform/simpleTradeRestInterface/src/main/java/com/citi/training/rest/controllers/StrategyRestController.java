package com.citi.training.rest.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.citi.training.data.entities.Strategy;
import com.citi.training.data.services.StrategyBs;

@RestController
@RequestMapping("/strategies")
public class StrategyRestController {
	
    @Autowired
    private StrategyBs strategyBs;

	public static final Logger log = LoggerFactory.getLogger(
			TradeRestController.class);
	
	@RequestMapping(
   			value = "/viewByStockName/{stock}",
		  	method = RequestMethod.GET,
		   	produces = {"application/json"})
    	public List<Strategy> viewByStockName(@PathVariable String stock) {
        	return strategyBs.viewByStockName(stock);
    	}
				
	@RequestMapping(
	    		value = "/strategyList",
	    		method = RequestMethod.GET,
	    		produces = {"application/json"})
    	public List<Strategy> getAll() {
        	return strategyBs.viewAllStrategies();
    	}	

	@RequestMapping(
			value = "/createStrategy",
			method = RequestMethod.POST,
			consumes = {"application/json"})
	@ResponseStatus(HttpStatus.CREATED)
	public int createTrade(@RequestBody Strategy strategy ) {
		log.debug("REST Controller rxd strategy:\n" + strategy.toString());
		return strategyBs.saveStrategy(strategy);
	}

}
