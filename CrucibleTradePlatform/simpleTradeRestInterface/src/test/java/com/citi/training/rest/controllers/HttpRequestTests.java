package com.citi.training.rest.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

/* Tests full stack, including servlet/web server */
/* These tests might access database */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestTests {

    public static final Logger log = LoggerFactory.getLogger(
                                                HttpRequestTests.class);

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test_createTrade_sanityCheck() throws Exception {
        String requestJson = TestUtil.testJson();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        log.debug("Sending REST request:\n" + requestJson);
        HttpEntity<String> entity = new HttpEntity<String>(requestJson,
                                                           headers);
        String result = restTemplate.postForObject(
                "http://localhost:" + port + "/trades", entity, String.class);

        log.debug("REST response: " + result);
        TestUtil.assertUnexpectedResult("HTTPRequestTest expected: ", TestUtil.testTrade,
                                        result);
    }

    @Test
    public void test_getAllTrades_sanityCheck() throws Exception {
        test_createTrade_sanityCheck();

        String result = this.restTemplate.getForObject(
                "http://localhost:" + port + "/trades", String.class);

        log.debug("Received Trades: " + result);
        TestUtil.assertUnexpectedResult("HTTPRequestTest expected: ", TestUtil.testTrade,
                                        result);
    }

    @Test
    public void test_getByStock_sanityCheck() throws Exception {
        test_createTrade_sanityCheck();

        String result = this.restTemplate.getForObject(
                "http://localhost:" + port + "/trades/" + TestUtil.testTrade.getStock(),
                String.class);

        log.debug("Received Trade: " + result);
        TestUtil.assertUnexpectedResult("HTTPRequestTest expected: ", TestUtil.testTrade,
                                        result);
    }
}
