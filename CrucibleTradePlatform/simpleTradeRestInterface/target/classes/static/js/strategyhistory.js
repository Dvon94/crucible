/**
 * 
 */
$(document).ready(startup);

function startup() {
	// hookup trade searching events
	$("#buttonGetAllStrategies").click(getAllStrategies);
	$("#buttonStrategiesByStock").click(getStrategyByStock);
}

function getAllStrategies() {

	$.ajax({
			url : "/strategies/strategyList",
			dataType : "json",
			type : "GET",
			cache : false,
			async : false,
			contentType : 'application/json; charset=utf-8',

			success : function(response) {

				var output = "<Table><thead><tr> <th>Initial Stock</th> <th>Starting Date</th> <th>End Date</th> <th>Initial Budget</th> <th>Starting Quantity</th> <th>Standard Devaition Set for Buy</th> <th>Standard Devaition Set for Sell</th></tr></thead>";
				for (var i=0; i < response.length; i++) {
					output+="<TR>";
					output+="<TD>" + response[i].stock + "</td>";
					output+="<TD>" + response[i].startDate + "</td>";
					output+="<TD>" + response[i].endDate + "</td>";
					output+="<TD>" + response[i].budget + "</td>";
					output+="<TD>" + response[i].startingQuantity + "</td>";
					output+="<TD>" + response[i].standardDeviationSale + "</td>";
					output+="<TD>" + response[i].standardDeviationBuy + "</td>";
					output+="</TR>";
					
			
				}
				output +="</Table>";
				$("#strategyHistoryTableFormat").html(output);
			},
			error : function(err) {
				alert("Error: " + err.responseText)
			}
		});
}

function getStrategyByStock() {

	var stockName = $("#textStockName").val();

	$.ajax({
		url : "/strategies/viewByStockName/" + stockName,
		dataType : "json",
		type : "GET",
		cache : false,
		async : false,
		contentType : 'application/json; charset=utf-8',

		
		success : function(response) {
			
			var output = "<Table><thead><tr> <th>Initial Stock</th> <th>Starting Date</th> <th>End Date</th> <th>Initial Budget</th> <th>Starting Quantity</th> <th>Standard Devaition Set for Buy</th> <th>Standard Devaition Set for Sell</th></tr></thead>";
			for (var i=0; i < response.length; i++) {
			
				output+="<TR>";
				output+="<TD>" + response[i].stock + "</td>";
				output+="<TD>" + response[i].startDate + "</td>";
				output+="<TD>" + response[i].endDate + "</td>";
				output+="<TD>" + response[i].budget + "</td>";
				output+="<TD>" + response[i].startingQuantity + "</td>";
				output+="<TD>" + response[i].standardDeviationSale + "</td>";
				output+="<TD>" + response[i].standardDeviationBuy + "</td>";
				output+="</TR>";
			}
			output +="</Table>";
			$("#strategyHistoryTableFormat").html(output);
		},
		error : function(err) {
			alert("Error: " + err.responseText)
		}
	});
}