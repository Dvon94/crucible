$(document).ready(startup);

function startup() {
	// hookup trade searching events
	$("#tradeHistoryIframe").hide();
	$("#strategyHistoryIframe").hide();
	
	$("#thIframe").click(TradeHIframe);
	$("#shIframe").click(StrategyHIframe);
}

function TradeHIframe() {
	$("#tradeHistoryIframe").show();
	$("#strategyHistoryIframe").hide();
}

function StrategyHIframe() {
	$("#strategyHistoryIframe").show();
	$("#tradeHistoryIframe").hide();
}

