DROP TABLE IF EXISTS trades;

create table trades (
    id int primary key not null auto_increment,
    tradeType varchar(5) not null,
    stock varchar(12) not null, -- could be limited to 4
    lastStateChange datetime(3) not null,
    state varchar(20) not null
);
