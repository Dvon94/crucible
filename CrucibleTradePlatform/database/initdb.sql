

-- -----------------------------------------------------
-- Table `initdb`.`strategy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `strategy` (
  `StrategyID` INT(11) NOT NULL AUTO_INCREMENT,
  `startingQuantity` INT(11) NOT NULL,
  `budget` DOUBLE NOT NULL,
  `startDate` DATETIME NOT NULL,
  `endDate` DATETIME NOT NULL,
  `stock` VARCHAR(5) NOT NULL,
  `standardDeviationSell` INT NOT NULL,
  `standardDeviationBuy` INT NOT NULL,
  PRIMARY KEY (`StrategyID`),
  UNIQUE INDEX `StrategyID_UNIQUE` (`StrategyID` ASC),
  INDEX `TradeID_idx` (`EndDate` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `initdb`.`trades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `trades` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tradeType` VARCHAR(5) NOT NULL,
  `stock` VARCHAR(12) NOT NULL,
  `size` INT(11) NOT NULL,
  `price` DOUBLE NOT NULL,
  `lastStateChange` DATETIME(3) NOT NULL,
  `state` VARCHAR(20) NOT NULL,
  `StrategyID` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `StrategyId_idx` (`StrategyID` ASC),
  CONSTRAINT `StrategyId`
    FOREIGN KEY (`StrategyID`)
    REFERENCES `strategy` (`StrategyID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
