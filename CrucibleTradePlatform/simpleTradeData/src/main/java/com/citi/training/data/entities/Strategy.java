package com.citi.training.data.entities;

import java.sql.Date;

public class Strategy {
	
	private int strategyId;
	private String stock;
	private Date startDate;
	private Date endDate;
	private double budget;
	private int startingQuantity;
	private int standardDeviationBuy;
    private int standardDeviationSell;

	
	
	
	public Strategy() {}

	public Strategy(
			//int strategyId,
			String stock,
			Date startDate, 
			Date endDate, 
			double budget,
			int startingQuantity,
			int standardDeviationBuy,
		    int standardDeviationSell

			) {
		//this.strategyId = strategyId;
		this.stock = stock; // comes from trades table
		this.startDate = startDate;
		this.endDate = endDate;
		this.budget = budget;
		this.startingQuantity = startingQuantity;
		this.standardDeviationSell = standardDeviationSell;
        this.standardDeviationBuy = standardDeviationBuy;
	}
	
	
	public int getStandardDeviationSell() {
		return standardDeviationSell;
	}

	public void setStandardDeviationSell(int standardDeviationSell) {
		this.standardDeviationSell = standardDeviationSell;
	}
	
	public int getStrategyId() {
		return strategyId;
	}

	public void setStrategyId(int strategyId) {
		this.strategyId = strategyId;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public int getStartingQuantity() {
		return startingQuantity;
	}

	public void setStartingQuantity(int startingQuantity) {
		this.startingQuantity = startingQuantity;
	}	


	public void setStandardDeviationBuy(int standardDeviationBuy) {
		this.standardDeviationBuy = standardDeviationBuy;
	}
	
	public int getStandardDeviationBuy() {
		return standardDeviationBuy;
	}
}
