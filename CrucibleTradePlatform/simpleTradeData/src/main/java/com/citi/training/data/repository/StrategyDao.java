package com.citi.training.data.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import com.citi.training.data.entities.Strategy;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

@Component
public class StrategyDao {

	private static final Logger logger = LogManager.getLogger(StrategyDao.class);
	
	private static final int SHORT_GAP_IN_SECONDS = 3;
	private static final int LONG_GAP_IN_SECONDS = 6;

	@Autowired
	private JdbcTemplate tpl;
	

	public Strategy getMostRecent() {
		return tpl.queryForObject("SELECT * FROM strategy ORDER  BY strategyId DESC LIMIT  1",
				new StrategyMapper());	
	}
	
	public List<Strategy> findbyStockName(String stock) {
        return tpl.query
        		("select * from strategy where stock=?", 
        				new Object[]{stock},
        				new StrategyMapper());
    }
	
	/*
	public List<Strategy> findAll () {
    	logger.info("Find all invoked");
        return tpl.query("select * from strategy as a join trades as b on a.strategyId = b.strategyId",
        		new StrategyMapper());
    }
	*/
	
	
	public List<Strategy> findAll () {
	    logger.info("Find all invoked");
	    return tpl.query("select * from strategy",
	        		     new StrategyMapper());
	}
		
			public List<Strategy> findbyStockName () {
		    	logger.info("Find all invoked");
		        return tpl.query("select * from strategy as a join trades as b on a.TradeID = b.id",
		        		new StrategyMapper());
		    }

	public int saveStrategy(Strategy strategy) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		this.tpl.update(
				new PreparedStatementCreator() {
					@Override
					public java.sql.PreparedStatement createPreparedStatement(java.sql.Connection con)
							throws SQLException {
						java.sql.PreparedStatement ps =
								con.prepareStatement(
								"insert into strategy"
								+ "(stock, startDate, endDate, budget, startingQuantity, standardDeviationSell,standardDeviationBuy )"
								+ "values (?, ?, ?, ?, ?, ?, ?)",
								Statement.RETURN_GENERATED_KEYS); 
						
						ps.setString(1, strategy.getStock());
						ps.setDate(2, strategy.getStartDate()); 
						ps.setDate(3, strategy.getEndDate());
						ps.setDouble(4, strategy.getBudget()); 
						ps.setInt(5, strategy.getStartingQuantity()); 
						ps.setInt(6, strategy.getStandardDeviationSell());
						ps.setInt(7, strategy.getStandardDeviationBuy());
						

						return ps;
					}
				},
				keyHolder);
		return keyHolder.getKey().intValue();
		//return 0;
	}

	private static final class StrategyMapper implements RowMapper<Strategy> {

		public Strategy mapRow(ResultSet rs, int rowNum) throws SQLException {
			Strategy strategy = new Strategy(
					rs.getString("stock"), 
					rs.getDate("startDate"), 
					rs.getDate("endDate"), 
					rs.getDouble("budget"), 
					rs.getInt("startingQuantity"), 
					rs.getInt("standardDeviationSell"),
					rs.getInt("standardDeviationBuy")
					);
			
					
			strategy.setStrategyId(rs.getInt("strategyID"));
					
			

			return strategy;
		}	    
	}

}
