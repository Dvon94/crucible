package com.citi.training.data.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.data.entities.Strategy;
import com.citi.training.data.repository.StrategyDao;

@Component
public class StrategyBs {

	@Autowired
	StrategyDao strategyDao;
	
	public Strategy viewMostRecent() {
		return strategyDao.getMostRecent();
		
	}
	
	public List<Strategy> viewByStockName(String stock) {
		return strategyDao.findbyStockName(stock);
		
	}
	
	public List<Strategy> viewAllStrategies() {
		return strategyDao.findAll();
	}
	
	public int saveStrategy(Strategy strategy) {
		if (
			strategy.getStartDate().equals("") || 
			strategy.getEndDate().equals("") ||
			strategy.getStartDate().before(new Date()) || 
			strategy.getEndDate().before(strategy.getStartDate())|| 
			strategy.getStartDate().after(strategy.getEndDate()) ||
			strategy.getStandardDeviationSell() > 3 ||
			strategy.getStandardDeviationBuy() > 3
			) {
			throw new IllegalArgumentException();
		} else {
			return strategyDao.saveStrategy(strategy);
		}
	}



}
