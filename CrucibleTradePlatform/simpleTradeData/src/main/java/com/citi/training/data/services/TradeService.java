package com.citi.training.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.data.entities.Trade;
import com.citi.training.data.repository.TradeRepository;

@Service
public class TradeService {

    @Autowired
    private TradeRepository tradeRepository;

    public Trade saveTrade(Trade newTrade) {
        return tradeRepository.saveTrade(newTrade);
    }

    public Trade createTrade(String stock, String tradeType, double price, int size, int strategyId) {
        return tradeRepository.saveTrade(new Trade(stock, price, size, tradeType, strategyId));
    }

    public Trade getTradeById(int id) {
        return tradeRepository.getTradeById(id);
    }

    public List<Trade> getTradesByStock(String stock) {
        return tradeRepository.getTradesByStock(stock);
    }

    public List<Trade> getTradesByState(Trade.TradeState state) {
        return tradeRepository.getTradesByState(state);
    }

    public List<Trade> getAllTrades() {
        return tradeRepository.getAllTrades();
    }
}
